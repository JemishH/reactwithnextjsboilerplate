import Head from 'next/head'
import { GlobalStyle, Container } from '../styles/pages/test.style'

export default function Home() {
  return (
    <>
      <Head>
        <title>website.com</title>
      </Head>
      <Container>
        <GlobalStyle />
        <h1>This is Landing Page</h1>
      </Container>
    </>
  )
}
