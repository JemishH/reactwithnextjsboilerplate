import styled, { createGlobalStyle } from 'styled-components'
import { mediaQueries } from '../../utils/mediaQuery'

const GlobalStyle = createGlobalStyle`
 h1 {
     margin:0;
   font-size: 4rem;
 }
`
const Container = styled.div`
  text-align: center;
  background-color: red;
  ${mediaQueries('md')`
    background-color: blue;
  `};
`

export { GlobalStyle, Container }
