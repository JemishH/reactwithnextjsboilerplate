import styled, { css } from 'styled-components'
import theme from './theme'
import { mediaQueries } from '../../utils/mediaQuery'

export const Para = styled.p`



`
export const Anchor = styled.a`
text-decoration:underline;`
export const Heading = styled.h1`
max-width: 318px;
color:${theme.color.white};
line-height:52px;
padding-left:77px;
`
export const HighLight = styled.span`
    color:${theme.color.primary}
`
export const Hashtag = styled.p`
font-size:16px;
color:${theme.color.orange}
`
export const SplashContent = styled.div`
position: absolute;
top: 0;
display: flex;
left: 0;
right: 0;
bottom: 0;
align-items: center;

`
export const Img = styled.img`
width:100%;
height:100%;
object-fit:cover;
`

export const LoginBg = styled.div`
background-color:${theme.color.loginBg};
height:100vh; 
padding-top:55px;
padding-bottom:55px;
`
export const Login = styled.div`
display: flex;
align-items: center; 
max-width:1230px;
border-radius:5px;
margin:0 auto;
height:100%;
box-shadow:${theme.color.loginBoxShadow};
background-color:${theme.color.white};
${mediaQueries('md')`
   box-shadow:none;
`};
`
export const LoginSplash = styled.div`
max-width:521px;
height:100%;
position:relative;
border-radius:5px;

${Img}{
    border-radius:5px 0 0 5px;
    object-position: right;
}
${mediaQueries('md')`
    display:none;
`};
`
export const LoginForm = styled.div`
flex: 1;
align-items: center;
justify-content: center;
display: flex;
${(props) => props.ThankYouPage
    && css`
  text-align:center;

`}

`
export const SignUp = styled.div`
width:283px;
`
export const LoginHeading = styled.h2`
    color:${theme.color.textColor};
    margin-bottom:30px;

    ${Para}{
        color:${theme.color.textLight};
        font-size:12px;
        ${(props) => props.ThankYouMessage
            && css`
          font-size:14px;
          font-family: "Roboto", sans-serif;
        
        `}
    }
`

export const Button = styled.button`
height:41px;
width:100%;
font-size:12px;
background-color:${theme.color.primary};
margin-bottom:20px;
display:flex;
align-items: center;
padding-left:20px;
border-radius:6px;
color:${theme.color.white};
font-weight:500;

${(props) => props.fbBtn
    && css`
    background-color: ${theme.color.fbbtn};
`}
${(props) => props.appleBtn
    && css`
    background-color: ${theme.color.black};
`}
${(props) => props.SignUP
    && css`
    text-align:center;
    height:47px;
    font-size:14px;
    font-weight:700;
    padding-left:0;
    display:block;
    font-family: "Roboto Condensed", sans-serif;
    margin-bottom:30px;
`}
svg{
    margin-right:12px;
}
`

export const LoginLink = styled.a`
font-size:12px;
text-decoration:underline;
color:${theme.color.textLight};
`
export const Form = styled.form``
export const ShowPassword = styled.div``
export const FormGroup = styled.div`
margin-bottom:15px;
position:relative;
svg{
    position: absolute;
    bottom: 15px;
    left: 20px;
    path{
        fill:${theme.color.primaryLight};
    }
}
${ShowPassword}{
    position: absolute;
    bottom: 15px;
    right: 20px; 
   svg{
       position:relative;
       bottom:-4px;
       left:inherit;
       
   }
}

${(props) => props.ValidationCheck
    && css`
    position:relative;
    ${Input}{
        padding-left:20px;
    }
    svg{
        position: absolute;
        right: 10px;
        top: 11px;
        left: auto;
    }
`}
`

export const CheckBoxSpan = styled.span`  
position: absolute;
top: -3px;
left: 0;
height: 20px;
width: 20px;
background-color: #eee;
border-radius:6px;

&:after{
    position:absolute;
    content:'';
    left: 8px;
    top: 5px;
    width: 5px;
    height: 9px;
    border: solid white;
    border-width: 0 2px 2px 0;
    transform: rotate(45deg);
    display:none;
}


`
export const Input = styled.input`
border:1px solid ${theme.color.border};
color:${theme.color.textColor2};
padding-left: 41px;

${(props) => props.Checkbox
    && css`
    opacity:0;
    cursor: pointer;
`}
${(props) => props.checked
    && css`
    ~ ${CheckBoxSpan}{
        background-color:${theme.color.orange};
        &:after{
            display:block;
        }
       
    }
`}
`
export const TermsCondition = styled.a`
margin-left:4px;
color:${theme.color.orange};
font-weight:500;
text-decoration: underline;
`
export const Label = styled.label`
color:${theme.color.labelColor};
margin-bottom:13px;
`

export const CustomCheckBox = styled.label`
position:relative;
padding-left:35px;
font-family: "Roboto", sans-serif;
font-size:12px;
text-transform:inherit;
font-weight:400;
`
export const ForgotPassword = styled.a`
font-size:12px;
color:${theme.color.textLight};
text-decoration:underline;
display:block;
text-align:right;
margin-bottom:30px;
`

export const SignUpLink = styled.div`
    color:${theme.color.textLight};
    font-size:12px;

    ${Anchor}{
        font-weight:500;
        color:${theme.color.orange};
        
    }
`
