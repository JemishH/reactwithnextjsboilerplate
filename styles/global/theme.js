export default {
  color: {
    primary: '#704AFB',
    primaryDark: '#562EED',
    primaryLight: '#C5CCE2',
    white: '#FFFFFF',
    orange: '#FD6C4D',
    green: '#69E0A4',
    black: '#000000',
    textColor: '#252A50',
    textColor2: '#71758B',
    textLight: '#999EBC',
    fbbtn: '#4567B2',
    labelColor: '#8389A5',
    border: '#BFC2DD',
    loginBg: '#F4F5FA',
    loginBoxShadow: '0 0 30px rgba(165, 172, 203, 27)',
  },
}
