export const secureKeys = {
  userData: 'userData',
  userToken: 'userToken',
  registerTab: 'registerTab',
  femaleList: 'femaleList',
  imageList: 'imageList',
}

export const localStorageKeys = {
  user: 'user',
  token: 'token',
  tab: 'tab',
  femaleData: 'femaleData',
  image: 'image',
}

export const defaultLocation = {
  latitude: 37.3324,
  longitude: -122.0304,
}
