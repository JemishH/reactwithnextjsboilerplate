export const validateGender = (values) => {
  const errors = {}

  if (!values.gender || !values.gender) {
    errors.gender = 'gender is required'
  } else {
    errors.gender = ''
  }
  return errors
}

export const validateName = (values) => {
  const errors = {}

  if (!values.name || !values.name) {
    errors.name = 'name is required'
  } else {
    errors.name = ''
  }
  return errors
}

export const validateBirthdate = (values) => {
  const errors = {}

  if (!values.birthdate || !values.birthdate) {
    errors.birthdate = 'birthdate is required'
  } else {
    errors.birthdate = ''
  }
  return errors
}

export const validateWhatsAppNumber = (values) => {
  const errors = {}

  if (!values.whatsAppNumber || !values.whatsAppNumber) {
    errors.whatsAppNumber = 'Number is required'
  } else {
    errors.whatsAppNumber = ''
  }
  return errors
}

export const validatePictures = (values) => {
  const errors = {}

  if (!values.image1) {
    errors.image1 = 'profile image is required'
  }
  // else if (values.image1) {
  //   errors.image1 = fileExtValidate(values.image1)
  // }
  else {
    errors.image1 = ''
  }
  if (!values.image2) {
    errors.image2 = 'profile image is required'
  }
  // else if (values.image2) {
  //   errors.image2 = fileExtValidate(values.image2)
  // }
  else {
    errors.image2 = ''
  }
  if (!values.image3) {
    errors.image3 = 'profile image is required'
  }
  // else if (values.image3) {
  //   errors.image3 = fileExtValidate(values.image3)
  // }
  else {
    errors.image3 = ''
  }

  return errors
}

export const validateBio = (values) => {
  const errors = {}

  if (!values.profileText || !values.profileText) {
    errors.profileText = 'bio is required'
  } else {
    errors.profileText = ''
  }
  return errors
}

const validExt = '.png, .jpeg, .jpg, .heic'
function fileExtValidate(fdata) {
  let error = ''
  const filePath = fdata.name
  const getFileExt = filePath
    .substring(filePath.lastIndexOf('.') + 1)
    .toLowerCase()
  const pos = validExt.indexOf(getFileExt)
  if (pos < 0) {
    error = 'File allow only (.png, .jpeg, .jpg, .heic) format.'
    return error
  }
  return (error = fileSizeValidate(fdata))
}

const maxSize = '2097152'
function fileSizeValidate(fdata) {
  let error = ''
  if (fdata.size) {
    const fsize = fdata.size
    if (fsize > maxSize) {
      error = 'Maximum file size should be 2mb.'
      return error
    }
    return error
  }
}
