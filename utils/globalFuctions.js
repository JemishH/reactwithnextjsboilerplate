import { localStorageKeys, secureKeys } from './Constant'

const jwt = require('jsonwebtoken')

export const getFormData = (object) => {
  const formData = new FormData()
  Object.keys(object).forEach((key) => formData.append(key, object[key]))
  return formData
}

export const encodeData = (data, key) => jwt.sign(data, key)

export const decodeData = (token, key) => {
  if (token) {
    return jwt.verify(token, key)
  }
  return null
}
export const getUserData = () => {
  if (typeof window !== 'undefined') {
    const userDataToken = localStorage.getItem(localStorageKeys.user)
    if (userDataToken) {
      const userData = decodeData(userDataToken, secureKeys.userData)
      return userData
    }
  }
  return null
}

export const getUserToken = () => {
  const user = getUserData()
  if (user && user.token) {
    return user.token
  }
  return ''
}

export const getDecodedData = (localKey, secureKey) => {
  if (typeof window !== 'undefined') {
    const encodedData = localStorage.getItem(localKey)
    if (encodedData) {
      const data = decodeData(encodedData, secureKey)
      return data
    }
  }
  return null
}

export default {
  getFormData,
  getUserToken,
}
