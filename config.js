// const localUrl = 'http://3.19.88.154:3000/api/' // old live server
const localUrl = 'http://3.133.171.28:1337/api/v1/' // new live server

const Config = {
  baseURL: localUrl,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeOut: 60000,
}

export default {
  Config,
}
